Release notes
=============

Scraper 0.2 (2017-04-03)
------------------------

**New features**

- custom exception ScraperError. Users should now ideally get relevant messages and changes that they should be looking at.
- representation for the Scraper object.
- proxies as a property
- requests client as a header
- no dependency on start date and end date
- support for custom logging from the users.
- csvfile will only be generated at the end of the scraping
- writing to csv possible with both a collection of records or a single record
- can push directly to ftp server
- api for xpath and css selector
- tree as a scraper property. This is to be used for xpath parsing
- soup as a scraper property. This is to be used for parsing using css selectors.

**Backwards-incompatible changes**

- headers that go into the csv will be `csvheaders`.

Scraper 0.1.1 (2017-03-27)
-------------------------

- issue #6 resolved: now will be able to just send parameters even when cookies are not present.
- issue #5 resolved: will be able to give the name  of the bot, or define a state for the bot. this will be used to create the csv file
- function get_standard_arg_parser defined and fixed in the utils. this can be called by the bot for defining the input parameters.
- a `save` method is provided in case the user needs to save some input to the csv file at any point in the code.

Scraper 0.1
-----------

First release of Scraper.

