=============
Bot Framework
=============

A framework that makes the creation of new bots and writing functions that parses the webpages simple and easy.

* Documentation: 
* Bitbucket: https://bitbucket.org/joydeep314/bots/src/3be13b58ab95/bot_framework/?at=joy.framework
* software: https://fix_the_license.com

Install
-------

To use this you can install this in your virtual environment:

    .. code-block:: bash

        pyvenv venv
        source venv/bin/activate
        cd bots/bot_framework
        python setup.py install

Run Example
-------
After environment setup go to localinsights-framework/common and run.

    .. code-block:: bash

        python example/el_dorado.py


Features
--------

Did someone say features?

* Cross-platform: Windows, Mac, and Linux are officially supported.

* Works with Python 3.5, and PyPy.

* will automaticall log all the transations and keep it in the log folder

* all gathered data will be kept in csv files in the csv/ folder.

* the Scraper class can be called both as a context manager and as a decorator.


How to use the framework and some sample code
---------------------------------------------


Create your Python bot and write the following code:

    .. code-block:: python

        from scraper import Scraper

        scraper = Scraper("bot")

        domain = "https://www.google.co.in/"
        context_root = ""

        scraper.start_date = "12/1/2016"
        scraper.end_date = "12/1/2016"

        scraper_args = {
            "page1": {
            "url": "{domain}/{context_root}".format(domain=domain,
                                                context_root=context_root1),
            "params": None
            }
        }

        @scraper.scrape(scraper_args["page1"]["url"])
        def parse_page1():
            data = str(scraper.response.content)
            return {"a": "Database" in data, "b": "B"}


        parse_functions = [parse_page1]
        scraper.headers = ["a", "b"]

        if __name__ == "__main__":
            scraper.run(parse_functions)

You can have a look at the sample examples in the `examples/` folder. That should give you a better idea on how to use the framework.

Testing:

To run preliminary tests you can just run

    python -m unittests discover

In case you just want to install the package to do some functional testing you can run either of the following commands:

    python setup.py develop
    pip install -e .

To look more into testing and have a more indepth testing report look into the testing docs in CONTRIBUTING.rst

