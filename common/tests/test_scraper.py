import json
import os
import pickle
import unittest
from unittest import mock
from unittest.mock import patch, Mock

from scraper import Scraper
from scraper.exceptions import ScraperError


def mock_functions():
    def a():
        for _ in range(1):
            yield {"head": 1}

    def b():
        for _ in range(1):
            yield {"head": 2}

    return [a, b]


class MockLogging:

    def info(self, *args, **kwargs):
        pass


class TestScraper(unittest.TestCase):

    response_file = 'tests/response_file.pickle'

    @patch('scraper.setup_logging', mock.Mock())
    def setUp(self):
        self.bot = Scraper("joy_bot",
                           start_date="1/7/1900",
                           end_date="1/8/1900")
        self.bot.start_date = "1/7/1900"
        self.bot.csvheaders = ["head"]

        self.domain = "http://tehamapublic.countyrecords.com"
        context_root = "scripts/hfweb.asp?formuser=public&Application=TEH"

        self.url = "{domain}/{context_root}".format(domain=self.domain,
                                                    context_root=context_root)
        self.bot.response = self._a_response_object()


    def _a_response_object(self):
        if os.path.isfile(self.response_file):
            with open(self.response_file, 'rb') as f:
                return pickle.load(f)
        else:
            with open(self.response_file, 'wb') as f:
                url = 'https://en.wikipedia.org/wiki/List_of_Nobel_laureates'
                response = self.bot.requests_response(url)
                pickle.dump(response, f, pickle.HIGHEST_PROTOCOL)
                return response

    def test_log_file(self):
        self.assertTrue(self.bot.log)

    def test_attrs(self):
        self.assertEqual(self.bot.name, "joy_bot", "namign")

    @patch('scraper.requests.get')
    def test_scrape_simple_page(self, mock_get):
        # Configure the mock to return a response with an OK status code.
        mock_get.return_value = 200

        @self.bot.scrape(self.url)
        def parse_page():
            data = self.bot.response
            return data

        self.assertEqual(parse_page(), 200, "response is ok")


    # We patch 'requests.post' with our own method. The mock object is passed in
    # to our test case method.
    @patch('scraper.requests.post')
    def test_scrape(self, mock_post):
        cookies = "mock cookie"
        mock_post.return_value = Mock(status_code=200,
                                      response=json.dumps({'key': 'value'}))

        url = "{domain}/scripts/hfweb.asp".format(domain=self.domain)
        params = {"Database": "TEHOR",
                  "x": "16",
                  "y": "16",
                  "FormUser": "public",
                  "APPLICATION": "TEH"}

        @self.bot.scrape(url, params=params, cookies=cookies)
        def parse_page():
            data = self.bot.response
            return data

        res = parse_page()

        self.assertEqual(res.status_code, 200, "response is ok")

    @patch('scraper.requests.post')
    def test_scrape_only_params_present(self, mock_post):
        """
        This test is to see if we can have something with passing only params
        and no cookies.
        """
        mock_post.return_value = Mock(status_code=200,
                                      response=json.dumps({'key': 'value'}))

        url = "{domain}/scripts/hfweb.asp".format(domain=self.domain)
        params = {"Database": "TEHOR",
                  "x": "16",
                  "y": "16",
                  "FormUser": "public",
                  "APPLICATION": "TEH"}

        # note that we are not passing any cookies here.
        @self.bot.scrape(url, params=params)
        def parse_page():
            data = self.bot.response
            return data

        res = parse_page()

        self.assertEqual(res.status_code, 200, "response is ok")

    def test_run(self):
        fn_list = mock_functions()
        self.bot.run(fn_list)
        res = self.bot.bot_last_status
        self.assertIn("1_7_1900.csv", res["path"])

        with open(res["path"]) as f:
            first_line = f.readline().rstrip()
            self.assertEqual(first_line, "head")

            second_line = f.readline().rstrip()
            self.assertEqual(second_line, "1")

            third_line = f.readline().rstrip()
            self.assertEqual(third_line, "2")


    def test_run_function_not_returning_dict(self):

        def will_not_return_dict():
            return "simple string"

        fn_list = [will_not_return_dict]

        # the below should raise an error because an invalid return statement
        # was passed into the method.
        self.assertRaises(ScraperError, self.bot.run, fn_list)

    def test_scraper_representation(self):
        scr = Scraper("sample", start_date='1/1/2017', end_date='1/1/2017')
        self.assertEqual(repr(scr), "Scraper('sample')")

    def test_set_logging_type(self):
        import scraper
        scraper.scraper_logging = 'file'
        bot = Scraper('bot')

        # now to test if the bot log file is present
        self.assertTrue(os.path.isfile('log/bot.log'))
        self.assertTrue(bot.log, 'log is now in file mode')

    def test_soup(self):
        self.assertTrue(self.bot.soup, "soup as a property")

    def test_find_element_by_css_selector(self):
        css_selector = 'table tr'
        expected = '1'
        result = self.bot.find_element_by_css_selector(css_selector)
        self.assertIn('Physics', result.get_text())

    def test_find_elements_by_css_selector(self):
        css_selector = 'table tr'
        expected = 143
        result = len(self.bot.find_elements_by_css_selector(css_selector))
        self.assertEqual(result, expected)

    def test_tree(self):
        self.assertTrue(self.bot.tree, "tree as a property for xpath")

    def test_find_element_by_xpath(self):
        # prepping
        # TODO: start date and end date to be removed
        scr = Scraper("joy_bot", start_date="something", end_date='something')
        url = 'http://econpy.pythonanywhere.com/ex/001.html'
        scr.response = scr.requests_response(url)
        xpath = '//div[@title="buyer-name"]/text()'
        expected = ''
        result = self.bot.find_element_by_xpath(xpath)

        # this test fails and will need to be fixed.
        self.assertEqual(result, expected,
                         'find the root cause of why this test fails')


if __name__ == "__main__":
    unittest.main()
